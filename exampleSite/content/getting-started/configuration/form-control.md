---
title: Form Control Template
type: full-width
draft: false
menu:
  sidenav:
    parent: Configuration
    weight: 3
---

# Form Control Template

{{< toc >}}

Form controls give you the ability to capture input from a user and send it to a backend (in our case, GitLab Pipelines). 

{{< figure src="../images/form-control-flow.png" class="w-full text-center" >}}

GitLab Pipelines can accept variables, that in turn determine what `jobs` should be run. This enables fast and simple ways to build workflow automations that rely _on some user input_, and it hides the complexity of _talking to your GitLab server and API_.

## Usage

Form controls are a special kind of UI element that can be embedded into your markdown pages. We use a [shortcode](https://gohugo.io/content-management/shortcodes/) to denote this:

```text
{{< literal text=`{{< form name="my-form" />}}` />}}
```

You can add multiple forms to a page, anywhere within the body, and they will be rendered according to how you configure them.

There are typically a few strategies to create a form, given your specific use case: 

 - [Raw HTML Inputs](../../examples/basics) - Simplest use case, capture form inputs and forward to GitLab Pipelines as variables.
 - [Form Generators](../../examples/json-forms) - This provides a way to auto generate forms and layouts from schemas using [JSON Forms](https://jsonforms.io/).
 - [Dynamic Content](../../examples/dynamic) - We can provide dynamic data and schemas for generating a form, invoking GitLab `jobs` and `artifacts`.
 - [Custom Layouts](../../examples/customize) - If you want more control over the form layout, styling and javascript we also provide a way to do that.

All the strategies above can be mixed and applied in any way, as they all share the same configuration settings.

## Configuration

The form control is uniquely identified by its `name` property. The name will be used to lookup settings under the `forms` parameter in the front matter. There are quite a few parameters and properties that can be set, below is the full list:

```yaml
---
forms:
  <form_name>:      # Uniquely identifies your form name
    task:           # Specifies the TASK_TO_RUN environment variable
    load:           # Specifies some settings to load dynamic metadata
      task:         # Name of the job to run, to generate initial metadata
      data:         # Path to data artifact created by the gitlab job
      schema:       # Path to schema artifact created by the gitlab job
      uischema:     # Path to uischema artifact created by the gitlab job
    extends:        # If set, you can extend another predefined form
    partial:        # If set, use the partial HTML layout file as the view
    class:          # Can be used to provide class names for styling
    scripts: []     # List of relative script paths to load when the form is loaded.
    data:           # Specify the initial data that will be given to the form
    scema:          # The data schema that is used to auto generate the JSON form
    uischema:       # The UI schema that is used to generate layout and validations
    debug:          # Shows additional debug info like data, schema, uischema
---
```

Some settings are also supported directly on the `{{< literal text=`{{< form ... />}}` />}}` itself as parameters:

```text
{{< literal text=`{{< form 
    name="my-form" 
    task="database:backup"
    class="p-4 border-2 border-blue-500"
    extends="database-backup"
    partial="databases/index.html"
    script="js/database.ts"
    debug="true"
/>}} ` />}}
```

## Form Customization

As mentioned, we also have a way to customize the form, with additional styling and any scripts it might need.

Given the example above, we load a script from the assets folder and specify the partial layout to be `databases/index.html`. 

```text
./pages
├── assets
|   └── js             
|       └── database.ts
└── layouts
    └── partials
        └── databases
            └── index.html
```

At build time, the page will be generated with the given customisations into the static page. All scripts will be compiled and minified automatically.

## Data driven forms

For some properties, such as `data`, `schema` and `uischema`, we support loading them from the `./pages/data` folder.

```text
└── data
    └── forms
        ├── my-form
        |   └── schema.yaml             // <- Form is generated from a schema file
        └── database-backup
            ├── data.json               // <- Initial form data to load
            ├── schema.json             // <- Data schema model for pipeline variables
            └── uischema.json           // <- UI layout and validation
```

This is particularly handy when using JSON Forms, and reduces the metadata defined in the page front matter.



