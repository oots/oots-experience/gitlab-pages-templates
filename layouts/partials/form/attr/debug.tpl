{{ $debug := false }}
{{ if .Get "debug" }}
{{ $debug = true }}
{{ end }}
{{ return $debug }}
