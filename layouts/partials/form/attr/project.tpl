{{ $value := .Get "project" | default $.Site.Params.workflow.project_id }}
{{ return $value }}