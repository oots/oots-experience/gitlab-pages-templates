import { FormFactoryGenerator } from "../factory";
import { ReactFormFactory } from "./react";

export function getFormFactory(): FormFactoryGenerator {
    return new ReactFormFactory();
}