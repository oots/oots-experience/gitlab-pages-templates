---
title: Folder structure
type: full-width
draft: false
menu:
  sidenav:
    parent: Getting Started
    weight: 10
---

# Folder structure explained

The contents of your `./pages` folder corresponds to the folders [used internally by hugo](https://gohugo.io/getting-started/directory-structure/). Hugo expects this set of sub folders, where it will find the relevant information to build and render your website.

The most relevant folders are listed below:

| Subfolder                 | Description                                                             |
| ------------------------- | ----------------------------------------------------------------------- |
| [**assets/**](./assets)   | Resources used by your website, such as images, styles and javascript   |
| [**config/**](./config)   | Additional configuration options used by hugo when building the website |
| [**content/**](./content) | This folder contains the actual pages that will make up your website.   |
| [**data/**](./data)       | Additional metadata available when generating the website.              |
| [**layouts/**](./layouts) | Custom HTML templates that can be used for more advanced use cases      |

For more information about each sub folder and what their purpouse is, see the sub sections for more details.

## Page hirarchy & site navigation

Web pages are typically defined as [markdown](https://www.markdownguide.org/) files in the `./pages/content` folder. These markdown files gets rendered into HTML web pages, with the folder structure corresponding to the website path strcuture and site navigation.

For example, consider the following folder structure, and how they map to pages on a website, eg `example.com`:

```yaml
# Current Folder: ./pages/content/

  about.md                                -> https://example.com/about
  contact.md                              -> https://example.com/contact
  blog/my-first-post.md                   -> https://example.com/blog/my-first-post
  playbooks/_index.md                     -> https://example.com/playbooks/
  playbooks/onboarding.md                 -> https://example.com/playbooks/onboarding
  playbooks/security/zero-day.md          -> https://example.com/playbooks/security/zero-day

```

Please note in the `playbooks/` example above that we have one special filename called `_index.md`. This is reserved by hugo to designate the "landing page", when no sub path is specified, in our case `https://example.com/playbooks/`.



## Seeded template folders

We have created a starter pack that looks something like below, containing the most relevant files with some descriptions:

```text
pages/                  # <-- This folder contains your website
  assets/               # <-- Define custom images, css & javascript (optional)
    images/             # <-- Images used by the website (incl. favicon.png)
    scss/               # <-- Supports any PostCSS compatible styles
    js/                 # <-- Javascript and/or Typescript
  config/               # <-- Additional configration overlays
  content/              # <-- Where you define the website contents
    getting-started/    # <-- A collection of documents to help you get started
    user/               # <-- Simple hooks for custom login/logout actions
    workflows/          # <-- Built-in workflow support for Gitlab pipelines
    _index.md           # <-- Home and landing page, uses special layout
    about.md            # <-- About page to tell the user what this website is about
  data/                 # <-- Metadata used to generate the website (optional)
    forms/              # <-- Declare forms as schemas, used in workflows
    social.json         # <-- Add ways that people can reach you and your team
    theme.json          # <-- Customize primary and secondary colors, fonts and more
  layouts/              # <-- If present, can be used to define additional partial templates
  hugo.yaml             # <-- Configure and customize the website
```
