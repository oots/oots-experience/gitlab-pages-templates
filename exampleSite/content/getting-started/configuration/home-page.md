---
title: Home Page Template
type: full-width
draft: false
menu:
  sidenav:
    parent: Configuration
    weight: 2
---

# Home Page Template

Included in the base templates of this website is a special kind of page, called the `Home Page`.

It provides a customisable home banner, as well as featured article sections to guide your users to a sub page.

This page is represented by the markdown file `./pages/content/_index.md`, and includes the following customizable parameters:

```yaml
# File: ./pages/content/_index.md
---
banner:               # Properties related to creating a banner for your home page
  title:              # The title for your landing page
  content:            # Subtext that will be displayed underneath the title
  image:              # Specify an image that will be displayed under the title and caption
  button:             # A call to action button that can be used to jump to the target page
    enabled:          # Only shows button if set to true
    label:            # The button label text
    link:             # The relative link or URL to a sub page
features: []          # A list of featured content to display underneath the banner
  - title:            # Title of the featured content
    image:            # Feature caption image if provided
    content:          # A short summarry of what this feature is about
    bulletpoints: []  # A list of bullet points to display under the text content
    button:           # If defined, this button would link to the featured content
      enable:         # Only shows button if set to true
      label:          # The button label text
      link:           # The relative link or URL to a sub page
---
```

### Home Banner

The home page `banner` gives you a quick and easy way to add a `title`, a `content` section, and a way to add buttons for call to action.

### Features

Featured sections can be used to draw the attention of the user to specific areas of your website, with a similar `title`, `content` and preview `image`, as well as actionable buttons.