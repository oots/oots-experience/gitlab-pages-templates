// Copyright (C) 2024 European Union
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
//
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { AppContext } from "./app";
import { PipelineInfo } from "./workflows/pipelines";
import { FormFactory } from "./forms/factory";
import { FormConfig } from "./forms/config";

export class FormController implements FormConfig {
  public data?: any;
  public schema?: any;
  public uischema?: any;

  public loading: boolean;
  public hasFooter: boolean;
  public pipeline?: PipelineInfo;
  private container: HTMLElement;
  private errors: any[];

  constructor(
    protected id: string,
    protected config: FormConfig
  ) { }

  get debug() {
    return this.config.debug;
  }

  isValid() {
    return !this.errors?.length;
  }

  dispatch(event: CustomEvent) {
    console.debug(event.type, [this.id], event.detail);
    window.dispatchEvent(event);
  }

  async bootstrap($el: HTMLElement, loadOptions?: any) {
    if (!$el) throw Error("Cannot initialise without an element $el");
    try {
      this.loading = true;

      // Check for existing form body content, then skip auto generate...
      const hasFormBody = $el?.innerHTML?.trim() !== "";
      if (hasFormBody) return await this.load(loadOptions);

      let { data, schema, uischema } = this.config || {};
      if (data || schema || uischema) {
        // Auto generate the form using known metadata
        const init = !loadOptions.task;
        await this.generateForm($el, schema, uischema, data, init);
      }

      // Load the initial metadata
      const res = await this.load(loadOptions);
      if (res) {
        // Regenerate the form using updated metadata
        data = res?.data || data;
        schema = res?.schema || schema;
        uischema = res?.uischema || uischema;
        await this.generateForm($el, schema, uischema, data, true);
      }

      if (!loadOptions?.task && !(data || schema || uischema)) {
        // No form metadata specified, display warning message
        this.setPlaceholder($el, `Form '${this.id}' is not defined`);
      }
    } catch (ex) {
      console.error('form:error', [this.id], ex.message);
      throw ex;
    } finally {
      this.loading = false;
    }
  }

  async reload(loadOptions: {
    task: string;
    data: string;
    schema: string;
    uischema: string;
  }) {
    try {
      this.loading = true;
      return await this.load(loadOptions, true);
    } catch (ex) {
      console.error('form:error', [this.id], 'Reload failed', ex.message);
      throw ex;
    } finally {
      this.loading = false;
    }
  }

  async load(
    options: {
      task: string;
      data: string;
      schema: string;
      uischema: string;
    },
    refresh: boolean = false
  ) {
    const { task } = options;
    const workflow = AppContext.Current?.Workflow;
    if (!workflow) throw new Error("WorkflowContext not available");
    if (!task) return null;
    console.debug("form:load", [this.id], { task, refresh, options });

    const artifact = async (job_id: number, path: string) => {
      if (!path) return undefined;
      return await workflow.Jobs?.getArtifact(job_id, path);
    };
    const extractArtifacts = async (job: any) => {
      if (!job?.artifacts) return {}
      const [data, schema, uischema] = await Promise.all([
        await artifact(job.id, options?.data),
        await artifact(job.id, options?.schema),
        await artifact(job.id, options?.uischema),
      ]);
      if (data) this.data = data;
      if (schema) this.schema = schema;
      if (uischema) this.uischema = uischema;
      return { data, schema, uischema };
    }

    // Try and find a job that was recently run and passed (if not explicitly asking for a refresh)
    const job = !refresh ? await workflow?.Jobs?.getLatest(task, { wait: true }) : null;
    if (job?.artifacts) {
      // Found a cached job with artifacts, fetch the relevant files
      const cached = await extractArtifacts(job);
      console.debug("form:load", [this.id], "Found cached artifacts...", { job, ...cached });
      return cached;
    }

    // No recent job with artifacts could be found, trigger a new task...
    console.debug("form:load", [this.id], "Starting a new task", { task });
    let pipeline = await workflow.Pipelines.start({
      TASK_TO_RUN: task
    });
    await workflow.Pipelines.waitFor(pipeline); // Wait for pipeline to finish

    // Once completed, find the job containing the status updates
    const jobs = await workflow.Pipelines.getJobs(pipeline.id);
    const latest = jobs?.find(({ name }) => name == task);
    if (latest?.status == "success") {
      const updated = await extractArtifacts(latest);
      console.debug("form:load", [this.id], "Init task completed...", { task, ...updated });
      return updated;
    }

    return null;
  }

  /**
   * Trigger a new pipeline using the captured inputs
   * @param $event The submit event generated by the target form element
   * @returns False is returned, to cancel the event from bubling upwards
   */
  async submit($from: HTMLFormElement, task_name: string) {
    // Collect the variables to attach to the request
    let variables = { ...(await this.getVariables($from)) };
    if (!variables["TASK_TO_RUN"] && task_name) {
      variables["TASK_TO_RUN"] = task_name;
    }
    Object.keys(variables).forEach((key) => {
      variables[key] = `${variables[key]}`;
    });

    // Build a request payload to trigger a pipeline
    const workflow = AppContext.Current.Workflow;
    if (!workflow) throw new Error("Workflows is not ready.");

    // Send the request to start a new pipeline and wait for a response
    console.debug("form.submit", variables);
    this.pipeline = await workflow.Pipelines.start(variables);
    console.debug("form.result", this.pipeline);

    if (this.pipeline?.id) {
      this.trackPipelineChanges();
    }

    return this.pipeline;
  }

  /**
   * Generates a form given the JSON Forms schema and uischema
   * @param $el The element to bind the form generation to
   */
  async generateForm($el: HTMLElement, schema: any, uischema: any, data: any = {}, init: boolean = true) {
    // Bind the form data to this controller
    this.schema = schema;
    this.uischema = uischema;
    this.data = data;

    // Create a child container element to work with (and remove old one if exists)
    const contents = document.createElement("div");
    if (this.container) this.container.innerHTML = ""; // Clear old contents
    this.container = $el;
    this.container.classList.add("my-8");
    this.container.appendChild(contents);

    const options = { schema, uischema, data, contents };
    this.dispatch(new CustomEvent("form:create", { detail: options }));

    // Generate a form using the UI framework factory provider (eg: React)
    const onChange = (data: any, errors: any[]) => {
      this.onData(data)
      this.onError(errors)
    };
    await FormFactory.Provider.generate(contents, options, onChange);

    // Detect if we have form control buttons to submit.
    // If no submit button found, attach a generic submit button
    const buttons = Array.from($el.querySelectorAll('[type="submit"]'));
    const noCtrls = !buttons || !buttons.length;
    this.hasFooter = noCtrls;

    // Trigger an event to inform external components that form is ready
    if (init) {
      const detail = { schema, uischema, data };
      this.dispatch(new CustomEvent("form:init", { detail }));
    }
  }

  async getVariables($form: HTMLFormElement) {
    // If the variables were already captured, we use them as-is (no need to go search for them)
    if (this.data) return this.data;

    // Alternatively, collect form controls that have a "name" attribute (eg: capture raw input vars)
    const inputs = Array.from($form.querySelectorAll("[name]"));
    const values = (inputs as HTMLInputElement[]).reduce(
      (agg: any, inp: HTMLInputElement) => ({ ...agg, [inp.name]: inp.value }),
      {}
    );

    console.debug(`form:vars`, [this.id], values);
    return values;
  }

  async trackPipelineChanges() {
    const { pipeline } = this;
    const context = AppContext.Current.Workflow;

    if (!pipeline) return;
    if (!context?.isPending(pipeline.status)) return;
    if (pipeline?.id) {
      // Pipeline is still busy, wait a few seconds and get a status update
      await new Promise((res) => setTimeout(res, 1000)); // Wait a second
      this.pipeline = await context.Pipelines.fetch(pipeline?.id);
      await this.trackPipelineChanges();
    }
  }

  private onData(data: any) {
    this.data = data;
    if (JSON.stringify(data) == JSON.stringify(this.data)) return;
    this.dispatch(new CustomEvent("form:data", { detail: data }));
  }

  private onError(errors: any[]) {
    this.errors = errors;
    if (!errors?.length) return;
    this.dispatch(new CustomEvent("form:error", { detail: errors }));
  }

  private setPlaceholder(
    $el: HTMLElement,
    message: string,
    color: string = "rgba(128, 128, 128, 0.25)"
  ) {
    if ($el) {
      $el.innerText = message;
      $el.style.color = color;
      $el.style.fontSize = "2rem";
      $el.style.textAlign = "center";
      $el.style.margin = "2rem auto";
    }
  }
}
