# Gitlab Page Templates

This repository contains a theme that is used to generate and publish a (static) website, using [Hugo](https://gohugo.io/documentation/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). 

 - [Installation Guide](./exampleSite/content/getting-started/installation.md)
 - [Configuring your website](./exampleSite/content/getting-started/configuration.md)
 - [Run locally in dev mode](./exampleSite/content/getting-started/_index.md#local-development)
 - [Folder structure explained](./exampleSite/content/getting-started/folders/_index.md)
 - [Publishing your website using GitLab](./exampleSite/content/getting-started/_index.md#publishing-your-website-using-gitlab)

## Simple and intuitive

To use this _website generator_, all you need to do is have the following folder structure (see [example site](./exampleSite/)):

```
pages/                  # <-- This folder contains your website
  assets/               # <-- Define custom images, css & javascript (optional)
  content/              # <-- Where you define the website contents
  data/                 # <-- Metadata used to generate the website (optional)
  hugo.yaml             # <-- Configure and customize the website
.gitlab-ci.yml          # <-- Add 'include' for '.gitlab/pages.yml' (used to publish)
```

This gives you a way to [create a content-based website](./exampleSite/content/getting-started/folders/_index.md), with zero effort (using `Markdown` and `YAML`), and have [Gitlab publish it for you](./exampleSite/content/getting-started/_index.md#publishing-your-website-using-gitlab). No additional infrastructure is required.

Check out our extensive [Getting Started](./exampleSite/content/getting-started/_index.md) guide for an in-depth look at all the features and functionality that is included.


## Features included

The following features are included with this [custom hugo theme](https://gohugo.io/hugo-modules/theme-components/):

 - Built-in Gitlab Authentication & Deployment
 - Write and update content in Markdown
 - Indexed Search Functionality
 - Dark Mode & Minimalistic Styling
 - Fully Responsive & Mobile Friendly
 - Page Tags & Categories
 - Support Contact Form
 - Syntax Highlighting
 - Pre-designed Pages
    - Homepage
    - Workflows
    - Search
    - About
    - Contact
    - Custom 404

## Tech Stack

We use the following technologies to build the websites.

- [Hugo](https://gohugo.io/)
- [Gitlab Ci](https://docs.gitlab.com/ee/ci/)
- [Markdown](https://markdownguide.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [PostCSS](https://postcss.org/)

Additional features might include [npm packages](https://www.npmjs.com/) and [hugo modules](https://gohugo.io/hugo-modules/).

## Contact and support

To report a bug, please check out the [Issues Page](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/issues). 
