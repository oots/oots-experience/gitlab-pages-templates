---
title: data
type: full-width
draft: false
menu:
  sidenav:
    parent: Folder structure
    weight: 10
---

# Pages / Data

The `./pages/data` folder is used mainly for keeping metadata that gets used when generating the static pages. 

It does not get published in the final static site, making it ideal for keeping `JSON`, `YAML`, `CSV` or `TOML` metadata that drives and generates this website.

```text
└── data
    ├── social.json                       // <- How to reach the owners
    ├── theme.json                        // <- Customize fonts and colors
    └── forms
        └── probe-dns
            ├── data.json                 // <- Initial values
            ├── schema.json               // <- Data schema and constraints
            └── uischema.json             // <- UI layout and validation

```

From `Hugo`'s point of view, it also **does not matter what format you use**, it will internally parse the data structure. For example, the well known file `social.json` can be replaced with a `social.yaml` or `social.toml` and it will still work.

## Form data

One type of metadata that we support is [JSON Forms Schemas](https://jsonforms.io/examples/basic), that can be used to dynamically generate form controls with inputs and validations. From data is located in `./pages/data/forms/<FORM_NAME>/`. 

The form data gets resolved using the `form` name, so in `{{< literal text=`{{< form name="example" />}}` />}}`, it will try and resolve `example` from `./pages/data/forms/example/(data|schema|uischema).(json|yaml|toml)`. For examples of how to use JSON forms, check the [example website samples](../../examples/json-forms).

