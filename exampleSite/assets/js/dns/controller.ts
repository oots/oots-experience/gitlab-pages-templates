import { AppContext } from 'js/app';
import { WorkflowContext } from 'js/workflows/context';

export class DnsController {
    loading = false;
    results = [];
    context?: WorkflowContext;

    constructor() {
        this.context = AppContext.Current.Workflow;
    }

    async fetchData(refresh = false) {
        let data = null;

        this.loading = true;

        // Define some mocked data or fetch from remote source
        // return await fetch('path-to-your/api/query');
        const domain = 'example.com';
        await new Promise((res) => setTimeout(res, 1000)); // Wait a second
        data = [
            { DNS_TARGET: domain, DNS_TEST_LB: true, PROBE_LIMIT: 10 },
            { DNS_TARGET: "dev." + domain, DNS_TEST_LB: false },
            { DNS_TARGET: "test." + domain, DNS_TEST_LB: false },
        ];
        this.loading = false;

        return data || [];
    }

    async submitChanges(item: any) {
        try {
            // Set row as busy
            item.loading = true;
            item.error = undefined;

            // Spawn the pipeline for the selected item
            const { context } = this;
            const pipeline = await context.Pipelines.start(item);

            // Wait for the pipeline to finish
            await context.Pipelines.waitFor(pipeline)
        } catch (ex) {
            console.error(ex.message);
            item.error = ex;
        } finally {
            item.loading = false;
        }
    }
}

// Expose global objects used by the frontend
declare global {
    interface Window { DnsController: typeof DnsController }
}
window.DnsController = DnsController;
