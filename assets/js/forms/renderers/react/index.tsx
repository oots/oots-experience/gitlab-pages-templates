// Copyright (C) 2024 European Union
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
//
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import React, { StrictMode, useState } from "react";
import { createRoot } from "react-dom/client";

import { FormFactoryGenerator } from "../../factory";
import MaterialFrom from "./material-ui";

export default function GenerateFrom({ schema, uischema, data, onData }) {
  const [captured, setData] = useState(data || {});
  const onChange = (values: any, errors: any[]) => {
    setData(values);
    onData && onData(values, errors);
  };

  if (uischema) {
    return (
      <MaterialFrom
        schema={schema}
        uischema={uischema}
        data={captured}
        setData={onChange}
      />
    );
  }
  
  let props = { data: captured, setData: onChange };
  if (schema) props = { ...props, ...{ schema } };
  if (uischema) props = { ...props, ...{ uischema } };
  return <MaterialFrom {...props} />;
}

export class ReactFormFactory implements FormFactoryGenerator {
  async generate(
    container: HTMLElement,
    options: {
      schema: any;
      uischema: any;
      data: any;
    },
    onChange: (data: any, errors: Error[]) => void,    
  ) {
    // Bind the react component to the newly created container and capture data
    const params = {
      ...options,
      onData: (data: any, errors: any[]) => onChange(data, errors),
    };
    const root = createRoot(container);
    root.render(
      <StrictMode>
        <GenerateFrom {...params} />
      </StrictMode>
    );
  }
}
