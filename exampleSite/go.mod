module example-site

go 1.20

require (
	code.europa.eu/oots/oots-experience/gitlab-pages-templates v0.0.0-20241018133904-ab5628b1d112 // indirect
	github.com/gethugothemes/hugo-modules/accordion v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/adsense v0.0.0-20240504032439-79fc09d96848 // indirect
	github.com/gethugothemes/hugo-modules/components/announcement v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/components/cookie-consent v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/components/custom-script v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/components/preloader v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/components/render-link v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/components/social-share v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/gallery-slider v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/gzip-caching v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/icons/font-awesome v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/images v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/modal v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/pwa v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/search v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/seo-tools/basic-seo v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/seo-tools/google-tag-manager v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/seo-tools/site-verifications v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/shortcodes/button v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/shortcodes/notice v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/tab v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/table-of-contents v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gethugothemes/hugo-modules/videos v0.0.0-20240925042433-d2b5d05977e8 // indirect
	github.com/gohugoio/hugo-mod-jslibs-dist/alpinejs/v3 v3.21300.20800 // indirect
	github.com/zeon-studio/hugoplate v0.0.0-20241002072641-c32ba3e0d751 // indirect
)
