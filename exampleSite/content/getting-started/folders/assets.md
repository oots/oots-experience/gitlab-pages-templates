---
title: assets
type: full-width
draft: false
menu:
  sidenav:
    parent: Folder structure
    weight: 10
---

# Pages / Assets

The `assets` folder contains additional resources that your website depends on when generating and rendering the web pages. 

Common asset types that you might encounter include:

 - [**Images**](#images) - The (un-optimized) images that will be used on your website. Images get compressed and resized automatically.
 - [**Scripts**](#scripts) - In some cases, you might need some custom javascript. This asset type gets compiled and minimized.
 - [**Styles**](#styles) - Stylesheets will enable you to customize the look and feel of the website. Stylesheets are compiled and minimized.

These resources will get compiled, transformed and optimized by **Hugo** and **PostCSS**, and put in your `./public` folder, ready to be published alongside your website's HTML. 


## Images

Images are an intregal part of your website. You use them for your branding, favicon and image captions on your pages.

Hugo will take care of formatting, compressing and optimising your images for the final website, if you place them in the assets folder. In addition, the images are also resized for all the required sizes (including thumbnails).


## Scripts

In some cases, you might require some custom scripts or functionality on your webpage. Hugo supports this by allowing you to add _scripts_ to your `assets` folder.

You can add vanilla `javascript`, `typescript`, or `react` source code files, and Hugo will automatically compile them, minimized and obfuscated. These scripts can import or require `npm` modules, that is declared in the `pages/package.json` file.

All scripts will be compiled to the `./public/js/` folder, as [configured here](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/blob/main/exampleSite/config/_default/config.yaml?ref_type=heads#L40):

```yaml
build:
  ...
  cachebusters:
    ...
    - source: assets/.*\.(js|ts|jsx|tsx)
      target: js
    ...
```



## Styles

Similar to how we can declare custom scripts for our webpages, we can also include custom styling. Hugo makes use of [PostCSS](https://gohugo.io/hugo-pipes/postcss/) for processing and compiling style sheets. We support `.css`, `.scss` and `.sass` formats.


All stylesheets will be compiled to the `./public/css` path, as [described and configured here](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/blob/main/exampleSite/config/_default/config.yaml?ref_type=heads#L42):

```yaml
build:
  ...
  cachebusters:
    ...
    - source: assets/.*\.(css|scss|sass)
      target: css
    ...

```