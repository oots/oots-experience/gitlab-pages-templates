import { getStore, setStore } from "../config";
import { apiFetch } from "../gitlab/api";

export class ProjectContext {
    get Cached(): any { return JSON.parse(getStore("workflow.projects") || "{}"); }
    set Cached(projects: any) { setStore("workflow.projects", JSON.stringify(projects)); }

    constructor(protected project_id: string) { }

    async load(project_id: string) {
        const cached = this.Cached;
        if (cached[project_id]) return cached[project_id];

        const { id, name, name_with_namespace, web_url } = await apiFetch(`/api/v4/projects/${project_id}/`);
        const metadata = { id, name, name_with_namespace, web_url };
        if (!metadata) return null;

        // Successfully got a response for the repository, persist details        
        await this.save(project_id, metadata);

        return metadata;
    }

    async save(project_id: string, metadata: any) {
        const cached = this.Cached;
        cached[project_id] = metadata;
        this.Cached = cached;
    }

    async delete(project_id: string) {
        let cache = this.Cached;
        delete cache[project_id];
        this.Cached = cache;
    }
}