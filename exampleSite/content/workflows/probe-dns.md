---
title: DNS Probes
categories: [support]
tags: [dns]
draft: false
forms:
  probe-dns:
    task: probe:dns # <-- Defines the TASK_TO_RUN pipeline ENV var
---

{{< form name="probe-dns" />}}