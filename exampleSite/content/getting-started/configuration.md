---
title: Configuration
type: full-width
draft: false
menu:
  sidenav:
    parent: Getting Started
    weight: 10
---

# Configuring your website

Everything in the website is customizable. Most of these settings can be found in your `hugo.yaml` file, under the `params` section, unless specified otherwise. There are a few basic settings that we should configure on a per-project basis:

- [Branding and Copyright Info](#branding-and-copyright-info) - Let users know what page they landed on, with context of your project  
- [Workflow Settings & Project ID](#workflow-settings--project-id) - Configure Gitlab backend details (eg: `code.europa.ec`) 
- [Page Template Settings](#page-template-settings) - Some page templates also have additional settings that can be set.
- [Search and Indexing](#search-and-indexing) - If you want to enable/disable full text searches on the website content
- [Additional Settings](#additional-settings) - Hugo also has a lot of additional configuration settings built in.

Also check out the [Authentication Flow](../configuration/gitlab-auth) document, to better understand how GitLab and the frontend website uses tokens to communicate with the backend, in the case of workflows.

Additional settings and parameters can be defined, and used in page templates. Have a look at the [official hugo configuration](https://gohugo.io/getting-started/configuration/) documentation for a complete overview of how settings get applied to the site and its pages.

## Branding and Copyright Info

In order to customize your website, we provide a few settings in the configuration file:

```yaml
# File: ./pages/hugo.yaml

title: YOUR_WEBSITE_NAME
...
params:  
  # You can customize your branding with the following settings...
  logo: images/ec-logo.png              # <-- Consider customizing
  logo_darkmode: images/ec-logo.png     # <-- Consider customizing
  logo_text: YOUR_BRAND_OR_DEPARTMENT_NAME
  ...

  # Update the copyright info for your web pages by ovverriding this
  copyright: Developed & maintained by the [YOUR_DEPARTMENT_NAME](https://code.europa.ec/YOUR_PROJECT_SLUG/)
```

Also take some time to update the [`./content/about.md`](../about.md) file to give a better personalized context about what your website is about. 


## Workflow Settings & Project ID

If you are planning to use integrated workflow templates, we need to configure some things. If you are not planning to use workflows, _you can skip this section_.

In order to know where to trigger Gitlab `pipelines` and `jobs`, the workflow engine needs to know your project ID and repository URL.

```yaml
# File: ./pages/hugo.yaml
...
params:  
  # Setup GitLab project for triggering pipelines
  workflow:
    apiURL: https://code.europa.eu
    project_id: '755' # <-- GitLab Repo -> [ ⋮ ] (top right) -> Copy project ID
    ...
```

You can also completely disable the workflows, by deleting the `./content/workflows` folder _with all its contents_.

## Page Template Settings

In addition to site-wide configuration, we also have page templates that have their own specific settings and parameters:

 - [Home Page Template](../configuration/home-page/)
 - [Workflow & Forms Template](../configuration/form-control/)

## Search and Indexing

In case you want to control _what_ gets indexed and is searchable on the website, you can also configure that. Alternatively, you can just `disable` search altogether. 

```yaml
# File: ./pages/hugo.yaml
...
params:
  ...
  # Enable search and indexing of certain sections of this web page
  search:
    enable: true
    include_sections: # <-- Search content from paths in `./pages/content/`
      - about
      - workflows
    ...
```

## Additional settings

Also check the [`./pages/config/`](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/tree/main/exampleSite/config/_default?ref_type=heads) folder for additional settings. These are generally set up with defaults for normal use cases.

The included theme is based on another theme called [hugoplate](https://github.com/zeon-studio/hugoplate), and it contains lots of additional settings that we will not cover in this page. 
