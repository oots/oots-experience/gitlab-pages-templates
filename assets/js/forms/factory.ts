import { getFormFactory } from "./renderers/default";

export interface FormFactoryGenerator {
    generate(
        container: HTMLElement,
        options: {
            schema: any;
            uischema: any;
            data: any;
        },
        onChange: (data: any, errors: Error[]) => void,
    ): Promise<any>;
}

export class FormFactory {
    static Provider: FormFactoryGenerator = getFormFactory();
}