import * as params from '@params';

import { FormController } from "./forms";
import { AppContext } from './app';


// Add our exported class(es) to the globals object
declare global {

  // Expose global objects used by the frontend
  interface Window {
    App: AppContext;

    FormController: typeof FormController;
  }
}

// Create a new app context using the provided params from hugo
window.App = new AppContext(params);
window.FormController = FormController;
