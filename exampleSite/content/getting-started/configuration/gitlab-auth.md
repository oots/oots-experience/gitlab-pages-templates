---
title: Gitlab Authentication
type: full-width
draft: false
menu:
  sidenav:
    parent: Configuration
    weight: 1
---

# Gitlab Authentication

Gitlab pages is part of your code repository, and behaves in the same way, meaning you can only access these pages if you have the permissions to the code repository. 

The minimum required permission _to view the pages_, is the `Reporter` role. With this role or higher, you can view the pages and search the knowledge base for articles, playbooks and workflows, but you cannot invoke any form controls.




## Workflow permissions and trigger tokens

If you plan to use features such as workflows and form controls, [the user also needs to be a maintainer](https://docs.gitlab.com/ee/ci/triggers/#create-a-pipeline-trigger-token) (eg: slightly elevated permissions from a developer), or it will throw an error.

The picture below shows the auth flow for invoking form controls, and the **orange indicators** show the additional steps required to get a user's [GitLab Trigger Token](https://docs.gitlab.com/ee/ci/triggers/#trigger-a-pipeline).

![GitLab Auth Flow](../images/gitlab-auth-flow.png)
 
The pipeline in GitLab will be triggered by the current user, but then control is given over to GitLab, as if its a normal pipeline that was invoked with a web trigger.

## Project ID and gitlab server URL

In order to know where to trigger Gitlab `pipelines` and `jobs`, the workflow engine needs to know your project ID and repository URL. This can be set in `hugo.yaml`:

```yaml
# File: ./pages/hugo.yaml
...
params:  
  # Setup GitLab project for triggering pipelines
  workflow:
    apiURL: https://code.europa.eu
    project_id: '755' # <-- GitLab Repo -> [ ⋮ ] (top right) -> Copy project ID
    ...
```