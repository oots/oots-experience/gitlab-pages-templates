---
title: config
type: full-width
draft: false
menu:
  sidenav:
    parent: Folder structure
    weight: 10
---

# Pages / Config

Hugo has some very good [configuration features built in](https://gohugo.io/getting-started/configuration/) that we can take advantage of.

Configuration can be defined on different levels:

 - [**Global Settings**](#global-settings) - The `hugo.yaml` applies to the entire site, unless overwritten on the next layers
 - [**Config Overlays**](#config-overlays) - We can also apply overlays from the `config` folder, eg: for `development` or `production`
 - [**Page Settings**](#page-settings) - These settings only apply to the current page and is declared as `front matter` at the top.


## Global Settings

For global and site-wide settings, we use the `./pages/hugo.yaml` file. A simple example would look something like this:

```yaml
# File: ./pages/hugo.yaml

title: ABC Widgets, Inc.
baseURL: https://example.org/
languageCode: en-us
params:
  subtitle: The Best Widgets on Earth
  contact:
    email: info@example.org
    phone: +1 202-555-1212
```

Take note of the `params` section, it's where we would typically define settings specific to your site. 

## Config Overlays

As mentioned above, configuration overlays are handy for conditionally applying settings for a `development` or `production` environment.

Another use case would be if we want to break our configuration into smaller more manageable chunks. Then we can use the `_default` folder with the filename being the property name, this will be applied on to of the `hugo.yml` configuration. 


For example, settings under the `params` property in the `hugo.yaml` file could be moved to a new file `./config/_default/params.yaml`, and it will apply the same settings.

```yaml
# File: ./pages/config/_default/params.yaml

subtitle: The Best Widgets on Earth
contact:
  email: info@example.org
  phone: +1 202-555-1212
```


## Page Settings

Similarly, we can also define settings or `params` on a specific page, using [front matter](https://gohugo.io/content-management/front-matter/). 

```yaml
# File: ./pages/content/example.md
---
title: Example
draft: false
weight: 10
params:
  subtitle: The Best Example on Earth
  author: John Smith
---
```

Hugo will use the site and page params when rendering and generating the pages.