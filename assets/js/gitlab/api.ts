import { parse as parseYAML } from 'yaml'
import { parseCSV } from './csv'
import { AppContext } from "../app";
import { resolveAuthProvider } from "../auth";
import { AuthSession } from "../auth/session";

export async function apiFetch(path: string, {
    method = 'GET',
    headers = {},
    session = AuthSession.Current,
    body = undefined as any,
} = {}) {
    const { api } = AppContext.Current?.Config || {};
    const action = `${api}${path}`;
    const provider = session?.ready ? await resolveAuthProvider(session.type) : null;
    if (session && provider) {
        // Apply the authentication headers
        headers = { ...headers, ...provider.headers(session) }
    }

    // Encode the body as a string (if not already a string)
    if (body && typeof body !== 'string') {
        body = JSON.stringify(body);
    }

    // Sanity check for SSRF 
    if (action.indexOf(api) !== 0) throw new Error('Not a valid API request');

    // Send the request to the api server
    const request = { method, headers, body };
    const res = await fetch(action, request)
        .then(checkResponse)
        .catch(errorResponse);

    // Return formatted data (if possible)
    const contentType = res.headers.get('Content-Type');
    switch (contentType) {
        case 'application/json':
            return await res.json();
        default:
            let data = await (res.bodyUsed ? res.body() : res.text())
            if (typeof data !== 'string') return data;

            // Check for well known file types and try to parse the data
            const ext = path.split('.').pop();
            switch (ext) {

                // Parse YAML data
                case 'yml': return parseYAML(data);
                case 'yaml': return parseYAML(data);

                // Parse the CSV data
                case 'csv': return parseCSV(data);

            }

            return data;
    }
}

export const checkResponse = async (response: any) => {
    if (!response?.ok) {
        // Custom message for failed HTTP codes
        const code = response.status;
        if (code === 404) throw new Error("404, Not found");
        if (code === 500) throw new Error("500, internal server error");
        // For any other server errors
        throw new Error(response.status);
    }
    return response;
}

export const jsonResponse = async (response: any) => {
    response = await checkResponse(response);
    return await response.json();
}

export const errorResponse = async (ex: Error) => {
    if (ex?.message) {
        console.error(ex.message, ex);
    }
    throw ex;
}

